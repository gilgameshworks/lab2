import numpy as np

from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix

class RecommenSys():

    def __init__(self):
        sc = SparkContext.getOrCreate()
        spark = SparkSession.builder.appName("Lab2").getOrCreate()
        self.parquet_df = spark.read.parquet('data/data.parquet')

    def TF_creation(self):
        hashingTF = HashingTF(inputCol="1", outputCol="tf", numFeatures=15)
        self.tf = hashingTF.transform(self.parquet_df)

    def IDF_creation(self):
        idf_model = IDF(inputCol="tf", outputCol="idf")
        self.idf = idf_model.fit(self.tf)
        self.idf_features = self.idf.transform(self.tf)

    def recommend(self, ordered_similarity, max_count=5):

        users_sim_matrix = IndexedRowMatrix(ordered_similarity)
        multpl = users_sim_matrix.toBlockMatrix().transpose().multiply(self.watched.toBlockMatrix())
        ranked_movies = multpl.transpose().toIndexedRowMatrix().rows.sortBy(lambda row: row.vector.values[0], ascending=False)
        result = []
        for i, row in enumerate(ranked_movies.collect()):
            if i >= max_count:
                break
            result.append((row.index, row.vector[0]))
        return result

    def infer(self):

        temp_matrix = IndexedRowMatrix(self.idf_features.rdd.map(
            lambda row: IndexedRow(row["users"], Vectors.dense(row["idf"]))
        ))
        temp_block = temp_matrix.toBlockMatrix()

        similarities = temp_block.transpose().toIndexedRowMatrix().columnSimilarities()
        user_id = 5

        filtered = similarities.entries.filter(lambda x: x.i == user_id or x.j == user_id)

        ordered_similarity = filtered.sortBy(lambda x: x.value, ascending=False) \
            .map(lambda x: IndexedRow(x.j if x.i == user_id else x.i, Vectors.dense(x.value)))

        recomendations = self.recommend(ordered_similarity, max_count=10)
        for movie_id, rank in recomendations:
            print(f'Movie #{movie_id} (rank: {rank})')

