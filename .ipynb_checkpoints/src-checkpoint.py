import numpy as np

from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.sql import SparkSession
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix


def recommend(ordered_similarity, max_count=5):

    users_sim_matrix = IndexedRowMatrix(ordered_similarity)
    multpl = users_sim_matrix.toBlockMatrix().transpose().multiply(watched.toBlockMatrix())
    ranked_movies = multpl.transpose().toIndexedRowMatrix().rows.sortBy(lambda row: row.vector.values[0], ascending=False)
    result = []
    for i, row in enumerate(ranked_movies.collect()):
        if i >= max_count:
            break
        result.append((row.index, row.vector[0]))
    return result

spark = SparkSession.builder.appName("MyApp").getOrCreate()

parquet_df = spark.read.parquet('data/data.parquet')

hashingTF = HashingTF(inputCol="1", outputCol="tf", numFeatures=15)
tf = hashingTF.transform(parquet_df)
#hashingTF.write().overwrite().save('data/tf')

idf_model = IDF(inputCol="tf", outputCol="idf")
idf = idf_model.fit(tf)

idf_model = IDF(inputCol="tf", outputCol="idf")
idf = idf_model.fit(tf)
idf_features = idf.transform(tf)

temp_matrix = IndexedRowMatrix(idf_features.rdd.map(lambda row: IndexedRow(row["users"], Vectors.dense(row["idf"]))))

temp_block = temp_matrix.toBlockMatrix()

similarities = temp_block.transpose().toIndexedRowMatrix().columnSimilarities()

user_id = np.random.randint(low=0, high=self.watched.numCols())

filtered = similarities.entries.filter(lambda x: x.i == user_id or x.j == user_id)

ordered_similarity = filtered.sortBy(lambda x: x.value, ascending=False) \
    .map(lambda x: IndexedRow(x.j if x.i == user_id else x.i, Vectors.dense(x.value)))

recomendations = self._get_recomendation(ordered_similarity)
for movie_id, rank in recomendations:
    print(f'- movie # {movie_id} (rank: {rank})')
